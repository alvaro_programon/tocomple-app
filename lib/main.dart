import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'package:tocomple_app/UbicationMap.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tocomple App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyLogoPage(title: 'Tocomple'),
      routes: <String, WidgetBuilder>{
        // Set routes for using the Navigator.
        '/home': (BuildContext context) => new MyHomePage(),
        '/logo': (BuildContext context) => new MyLogoPage()
      },
    );
  }
}

class MyLogoPage extends StatefulWidget {
  MyLogoPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyLogoPageState createState() => _MyLogoPageState();
}

class _MyLogoPageState extends State<MyLogoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
        decoration: BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Colors.green,
                Colors.deepOrange
              ],
              begin: Alignment(0.5, -1.0),
              end: Alignment(0.5, 1.0)
          ),
        ),
        child: new Center(
          child: new SingleChildScrollView(
            padding: const EdgeInsets.all(36.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    //height: 300.0,
                    child: Image.asset(
                      "images/diego.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(height: 35.0,),
                  Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(30.0),
                    color: Colors.green,
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed('/home');
                      },
                      child: Text("Empezar",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: 'Montserrat', fontSize: 20.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold)
                      ),
                    ),
                  ),
                ]
            ),
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  var markersId = 0;
  var currentLocation = LocationData;
  Completer<GoogleMapController> _controller = Completer();

  final emailFieldController = TextEditingController();
  final paymentFieldController = TextEditingController();
  final commentaryFieldController = TextEditingController();

  CameraPosition _kGooglePlex = CameraPosition(//santiago por defecto
    target: LatLng(-33.443646,-70.649415),
    zoom: 15
  );

  void _add(UbicationMap ubicationMap)
  {
    var markerIdVal = markersId.toString();
    markersId++;

    final MarkerId markerId = MarkerId(markerIdVal);
    // creating a new MARKER
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(
        double.parse(ubicationMap.latitude),
        double.parse(ubicationMap.longitude),
      ),
      infoWindow: InfoWindow(title: ubicationMap.name, snippet: ubicationMap.description),
      onTap: () {
        print('CLICK InfoWindow');
        _onMarkerTapped(markerIdVal,ubicationMap);
      },
    );

    setState(() {
      // adding a new marker to map
      markers[markerId] = marker;
    });
  }

  void _onMarkerTapped(String markerId,UbicationMap ubicationMap)
  {
    print('_onMarkerTapped markerId '+markerId);
    _commentUbication(markerId,ubicationMap);
  }

  Future<void> _commentUbication(String markerId,UbicationMap ubicationMap) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text('Comentar '+ubicationMap.name),
            contentPadding: EdgeInsets.all(20.0),
            children: <Widget>[
              SizedBox(height: 20.0),
              TextField(
                controller: emailFieldController,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 18.0,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Email",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                        12.0
                    ),
                    borderSide: BorderSide(
                        color: Colors.green,
                        width: 2,
                        style: BorderStyle.none
                    ),
                  ),
                  hintStyle: TextStyle(
                    color: Colors.green[300],
                  ),
                  prefixIcon: const Icon(
                    Icons.person,
                    color: Colors.green,
                  ),
                  labelText: "Correo Electrónico",
                ),
              ),
              SizedBox(height: 30.0),
              TextField(
                controller: commentaryFieldController,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 18.0,
                ),
                maxLength: 200,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Ingrese su comentario",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                        12.0
                    ),
                    borderSide: BorderSide(
                        color: Colors.green,
                        width: 2,
                        style: BorderStyle.none
                    ),
                  ),
                  hintStyle: TextStyle(
                    color: Colors.green[300],
                  ),
                  prefixIcon: const Icon(
                    Icons.comment,
                    color: Colors.green,
                  ),
                  labelText: "Comentario",
                ),
              ),
              SizedBox(height: 20.0),
              TextField(
                controller: paymentFieldController,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 18.0
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Cómo pagó usted?",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                        12.0
                    ),
                    borderSide: BorderSide(
                        color: Colors.green,
                        width: 2,
                        style: BorderStyle.none
                    ),
                  ),
                  hintStyle: TextStyle(
                    color: Colors.green[300],
                  ),
                  prefixIcon: const Icon(
                    Icons.monetization_on,
                    color: Colors.green,
                  ),
                  labelText: "Método de pago",
                ),
              ),
              SizedBox(height: 40.0),
              Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(30.0),
                color: Colors.green,
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () {
                    if(emailFieldController.text=="" || paymentFieldController.text=="" || commentaryFieldController.text=="")
                    {
                      _showDialog("Por favor","ingrese todos los datos.");
                    }
                    else
                    {
                      _fetchCreateCommentary(ubicationMap.id.toString(),emailFieldController.text,commentaryFieldController.text,paymentFieldController.text);
                    }
                    //Navigator.of(context).pushReplacementNamed('/home');
                  },
                  child: Text("Enviar Comentario",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Montserrat', fontSize: 20.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)
                  ),
                ),
              )
            ],
          );
        }
    );
  }

  _fetchCreateCommentary(ubication_id,email,commentary,payment) async
  {
    print('ubication_id '+ubication_id);
    print('email '+email);
    print('commentary '+commentary);
    print('payment '+payment);
    http.post("http://35.202.128.64/comentary",
        body: {'ubication_id': ubication_id, 'description': commentary,'email': email,'payment': payment})
        .then((result) {
      if (result.statusCode == 200)
      {
        /*list = (json.decode(response.body) as List)
          .map((data) => new Photo.fromJson(data))
          .toList();*/
        Map data = jsonDecode(result.body);

        if(data["state"]=="OK")
        {
          _showDialog("Excelente","Comentario Enviado!");
        }
        else {
          _showDialog("Error","Ocurrió un error...");
        }
      }
      else {
        _showDialog("Error","Ocurrió un problema con la conectividad, intente nuevamente por favor.");
      }
      return result.body;
    })
        .catchError((err) => _showDialog("Error","Ocurrió un problema con la conectividad, intente nuevamente por favor."))
        .whenComplete(() { });
  }

  @override
  void initState() {
    _getLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
          _fetchMarkers();
        },
        scrollGesturesEnabled: true,
        tiltGesturesEnabled: true,
        rotateGesturesEnabled: true,
        myLocationEnabled: true,
        compassEnabled: true,
        myLocationButtonEnabled: true,
        markers: Set<Marker>.of(markers.values),
      ),
      /*floatingActionButton: FloatingActionButton.extended(
        //onPressed: _fetchMarkers,
        label: Text('To the lake!'),
        icon: Icon(Icons.directions_boat),
      ),*/
    );
  }

  Future _fetchMarkers() async
  {
    http.get("http://35.202.128.64/getUbications",
        //body: {'user': user, 'pass': pass})
    ).then((result) {
      print('RESULT : '+result.body);
      if (result.statusCode == 200)
      {
        Map data = jsonDecode(result.body);
        List<dynamic> list = List();
        list = data["result"].map((result) => new UbicationMap.fromJson(result)).toList();

        for(int b=0;b<list.length;b++)
        {
          UbicationMap ubicationMap = list[b] as UbicationMap;
          _add(ubicationMap);
        }
      }
      else {
        _showDialog("Error","Ocurrió un problema con la conectividad, intente nuevamente por favor.");
      }
      return result.body;
    })
        .catchError((err) => _showDialog("Error","Ocurrió un problema con la conectividad, intente nuevamente por favor. "+err.toString()))
        .whenComplete(() { });
  }

  Future<void> _goToThePosition(lat,lon) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
            //bearing: 192.8334901395799,
            target: LatLng(lat,lon),
            //tilt: 59.440717697143555,
            zoom: 18,
    )));
  }

  _getLocation() async {
    var location = new Location();
    try
    {
      currentLocation = await location.getLocation().then((data)
          {
            _goToThePosition(data.latitude,data.longitude);
          }
      );

      location.onLocationChanged().listen((LocationData currentLocation) {
        //print('onLocationChanged');
        //print(currentLocation.latitude);
        //print(currentLocation.longitude);
        //setState(() {
        //
        //  _kGooglePlex = CameraPosition(
        //    target: LatLng(currentLocation.latitude,currentLocation.longitude),
        //  );
        //  //_add(currentLocation.latitude,currentLocation.longitude);
        //});
      });

      setState(() {}); //rebuild the widget after getting the current location of the user
    } on Exception {
      currentLocation = null;
    }
  }

  void _showDialog(title,message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Aceptar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
