class UbicationMap
{
  final int id;
  final int user_id;
  final String name;
  final String latitude;
  final String longitude;
  final String description;
  final String created_at;
  final String updated_at;

  UbicationMap({
    this.id,
    this.user_id,
    this.name,
    this.latitude,
    this.longitude,
    this.description,
    this.created_at,
    this.updated_at
  });

  factory UbicationMap.fromJson(Map<String, dynamic> json) {
    return new UbicationMap(
      id: json['id'],
      user_id: json['user_id'],
      name: json['name'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      description: json['description'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
    );
  }
}